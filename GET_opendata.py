import requests
import csv
import os

working_dir = os.getcwd()
data_dir = working_dir+'/opendata/'

# download opendata via GET API
def get_rawdata():
    for i in range(102, 108):
        for j in range(1,5):
            season = str(i)+'S'+str(j)

            # 租賃
            url = 'https://plvr.land.moi.gov.tw//DownloadSeason?season='+season+'&fileName=A_lvr_land_C.csv'
            outputfile = data_dir+season+'_A_lvr_land_C.csv'
            print(outputfile)

            res=requests.get(url)
            if res.status_code == 200:
                with open(outputfile, 'w', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    data = res.text
                    for row in data.split('\r\n'):
                        if len(row.split(',')) == 29:
                            writer.writerow(row.split(','))

            # 買賣
            url = 'https://plvr.land.moi.gov.tw//DownloadSeason?season='+season+'&fileName=A_lvr_land_A.csv'
            outputfile = data_dir+season+'_A_lvr_land_A.csv'
            print(outputfile)

            res=requests.get(url)
            if res.status_code == 200:
                with open(outputfile, 'w', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    
                    data = res.text
                    for row in data.split('\r\n'):
                        if len(row.split(',')) == 28:
                            writer.writerow(row.split(','))

            # 新建案
            url = 'https://plvr.land.moi.gov.tw//DownloadSeason?season='+season+'&fileName=A_lvr_land_B.csv'
            outputfile = data_dir+season+'_A_lvr_land_B.csv'
            print(outputfile)

            res=requests.get(url)
            if res.status_code == 200:
                with open(outputfile, 'w', newline='') as csvfile:
                    writer = csv.writer(csvfile)
                    data = res.text
                    for row in data.split('\r\n'):
                        if len(row.split(',')) == 28:
                            writer.writerow(row.split(','))
def concat_data():
    df_rent = pd.DataFrame()
    df_buy_new = pd.DataFrame()
    df_buy_old = pd.DataFrame()

    for i in range(102, 108):
        for j in range(1,5):
            season = str(i)+'S'+str(j)

            # 臺北市不動產租賃
            url = 'https://plvr.land.moi.gov.tw//DownloadSeason?season='+season+'&fileName=A_lvr_land_C.csv'
            file = data_dir+season+'_A_lvr_land_C.csv'

            df1 = pd.read_csv(file)
            df1=df1[1:]
    #         print(len(df_rent),len(df1))
            df_rent = pd.concat([df_rent, df1])
            if len(df_rent.drop_duplicates()) != len(df_rent):
                print(file,'有重複!')

            # 臺北市不動產買賣
            url = 'https://plvr.land.moi.gov.tw//DownloadSeason?season='+season+'&fileName=A_lvr_land_A.csv'
            file = data_dir+season+'_A_lvr_land_A.csv'

            df2 = pd.read_csv(file)
            df2=df2[1:]
    #         print(len(df_buy_old),len(df2))
            df_buy_old = pd.concat([df_buy_old, df2])
            if len(df_buy_old.drop_duplicates()) != len(df_buy_old):
                print(file,'有重複!')

            # 臺北市新建案買賣
            url = 'https://plvr.land.moi.gov.tw//DownloadSeason?season='+season+'&fileName=A_lvr_land_B.csv'
            file = data_dir+season+'_A_lvr_land_B.csv'

            if os.path.isfile(file): # 有些年度沒有新建案
                df3 = pd.read_csv(file)
                df3=df3[1:]
    #             print(len(df_buy_new),len(df3))
                df_buy_new = pd.concat([df_buy_new, df3])
                if len(df_buy_new.drop_duplicates()) != len(df_buy_new):
                    print(file,'有重複!')
    print(len(df_rent), len(df_buy_old), len(df_buy_new))
    
    # output csv files
    df_rent.to_csv("102S1-108S1_rentdata.csv", index=False)
    df_buy_old.to_csv("102S1-108S1_buyolddata.csv", index=False)
    df_buy_new.to_csv("102S1-108S1_buynewdata.csv", index=False)
    
if __name__ == '__main__':
    get_rawdata()
    print('Downloaded.')
    concat_data()    
    print('Output 3 csv files.')