# 591 rent parser

A parser for 591 rent website.

## Library

* selenium
* BeautifulSoup
* requests

需安裝 chrome driver

## 說明

1.  爬取 591 租屋網台北市的租屋物件
2.  輸出一個 csv 檔，含以下欄位：
    *  地址
    *  租金
    *  坪數
    *  樓層 (所在樓層/總樓層)
    *  型態
    *  現況 (分租套房/獨立套房/雅房 etc.)
    *  格局 (電梯大樓/透天厝 etc.)
    *  車位


## GET opendata
*  透過API GET抓取內政部時價登陸網 102-108 年資料，含:
   * 不動產租賃
   * 不動產中古屋買賣
   * 不動產新建案買賣

## Analytics

使用視覺化套件實作可視化分析圖表

* Library
  * geopandas
  * matplotlib
  * seanborn
